#!/bin/sh

set -x

export NAME=$(basename $PWD)
export TAG=latest

docker build -t $NAME:$TAG .

docker run -it --rm \
    --network dcs \
    --name $NAME \
    --hostname $NAME \
    -v $NAME:/srv \
    $* \
    $NAME:$TAG 

#docker image prune -a -f

