FROM	alpine:3.17
LABEL	maintainer=Dis4sterRec0very

#
# Setup environment
#
ENV	DOMAIN=${DOMAIN:-mydcs}
ENV	TLD=${TLD:-dev}
ENV	ADMIN_PASSWORD=${ADMIN_PASSWORD:-secret}
ENV	LDAPDEBUG=none
ENV 	LDAPURI="ldapi:/// ldap:///"

#
# Install OpenLDAP and arrange directory structure
#
RUN	set -euxo pipefail \
	; apk --no-cache --update add curl htop unbound python3 py3-pip \
	; pip3 install socrate \
	; curl -so /etc/unbound/root.hints https://www.internic.net/domain/named.cache \
	; chown root:unbound /etc/unbound \
	; chmod 775 /etc/unbound \
	; /usr/sbin/unbound-anchor -a /etc/unbound/trusted-key.key || true

#
# Copy utility scripts including docker-entrypoint.sh to image
#
COPY	img_fs /

EXPOSE 	53/tcp

HEALTHCHECK CMD dig @127.0.0.1 || exit 1

CMD	/start.py
