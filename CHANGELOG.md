<a name="unreleased"></a>
## [Unreleased]


<a name="0.0.1"></a>
## 0.0.1 - 2023-08-18
### Feat
- Initial release, borrowed from mailu


[Unreleased]: https://gitlab.com/mydcs/devops/compare/0.0.1...HEAD
