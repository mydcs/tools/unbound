# Status

[gitlab-pipeline-image]: https://gitlab.com/mydcs/tools/unbound/badges/main/pipeline.svg
[gitlab-pipeline-link]: https://gitlab.com/mydcs/tools/unbound/-/commits/main
[gitlab-release-image]: https://gitlab.com/mydcs/tools/unbound/-/badges/release.svg
[gitlab-release-link]: https://gitlab.com/mydcs/tools/unbound/-/releases
[gitlab-stars-image]: https://img.shields.io/gitlab/stars/mydcs/tools/unbound?gitlab_url=https%3A%2F%2Fgitlab.com
[gitlab-stars-link]: https://hub.docker.com/r/mydcs/tools/unbound

[docker-pull-image]: https://img.shields.io/docker/pulls/mydcs/unbound.svg
[docker-pull-link]: https://hub.docker.com/r/mydcs/unbound
[docker-release-image]: https://img.shields.io/docker/v/mydcs/unbound?sort=semver
[docker-release-link]: https://hub.docker.com/r/mydcs/unbound
[docker-stars-image]: https://img.shields.io/docker/stars/mydcs/unbound.svg
[docker-stars-link]: https://hub.docker.com/r/mydcs/unbound
[docker-size-image]: https://img.shields.io/docker/image-size/mydcs/unbound/latest.svg
[docker-size-link]: https://hub.docker.com/r/mydcs/unbound


[![gitlab-pipeline-image]][gitlab-pipeline-link] 
[![gitlab-release-image]][gitlab-release-link]
[![gitlab-stars-image]][gitlab-stars-link]


[![docker-pull-image]][docker-pull-link] 
[![docker-release-image]][docker-release-link]
[![docker-stars-image]][docker-stars-link]
[![docker-size-image]][docker-size-link]

# How To

Unbound ist ein DNS-Cache.


## Config

Das Netzwerk ist entweder über den Parameter SUBNET zu definieren, oder aber es wird die Config selbst erstellt.

### Beispielconfig

```
server:
  verbosity: 1
  interface: 0.0.0.0
  logfile: ""
  do-ip4: yes
  do-ip6: no
  do-udp: yes
  do-tcp: yes
  do-daemoniwwwze: no
  access-control: 172.22.14.0/24 allow
  directory: "/etc/unbound"
  username: unbound
  auto-trust-anchor-file: trusted-key.key
  root-hints: "/etc/unbound/root.hints"
  hide-identity: yes
  hide-version: yes
  cache-min-ttl: 300
```

## Aufruf

### CLI

```
docker run -it --rm -p 53:53 -e SUBNET=${SUBNET:-172.22.14.0/24} mydcs/unbound:latest
```

### Compose

```dockerfile
version: "3.8"

services:
  unbound:
    container_name: unbound
    hostname: unbound
    restart: always
    environment:
      - SUBNET=${SUBNET:-172.22.14.0/24}
    networks:
      - default
    ports:
      - 53:53

networks:
  default:
    name: iaas
```
